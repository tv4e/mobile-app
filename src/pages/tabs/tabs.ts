import { Component } from '@angular/core';
import { HomePage } from '../home/home';
import { Recommendations } from '../recommendations/recommendations';
import { Help } from '../help/help';
import { NavController } from 'ionic-angular';
import { SpeechRecognition } from "@ionic-native/speech-recognition";
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import _ from 'lodash';

@Component({
  templateUrl: 'tabs.html'
})

export class TabsPage {

  page1: any = HomePage;
  page2: any = Recommendations;
  matches: String [];
  serial: string;

  constructor(public navCtrl:NavController,
              private speechRecognition: SpeechRecognition,
              private storage: Storage,
              public httpClient: HttpClient)
  {
    storage.get("serial").then((val) => {
      this.serial = val;
    })
  }

  public openHelp(){
    this.navCtrl.push(Help);
  }

  public voiceCommand() {

    this.speechRecognition.hasPermission().then(permission => {
      if(!permission){
        this.speechRecognition.requestPermission().then( () => {this.listen();});
      } else{
        this.listen();
      }
    })
  }

  public listen(){
    let options = {
      language: 'pt-PT',
      matches: 1,
      prompt: 'Estou à escuta',
      showPopup: true,
      showPartial: true
    }

    this.speechRecognition.startListening(options)
      .subscribe(matches => {
        this.matches = matches;
        this.postMatches();
      })
  }
  public postMatches (){
    let link = 'http://api_mysql.tv4e.pt/api/sendKey/' + this.serial;
    let keyCode = "";

    let keys = {
      49:['rtp1', 'rtp', '1', 'um'],
      50:['rtp2', '2', 'dois'],
      51:['sic', '3', , 'tres'],
      52:['tvi', ,'fátima','fátima lopes', '4', 'quatro'],
      53:['notícias', 'sic notícias', '5', 'cinco'],
      54:['rtp3', '6', 'seis'],
      55:['memória', '7', 'sete'],
      56:['la sexta', 'sexta', '8', 'oito'],
      57:['antena 3', 'antena', '9', 'nove'],
      40:['cima', 'próximo'],
      38:['baixo', 'anterior'],
      48:['biblioteca', '0', 'zero']
    }

    _.each(keys,(value, _keyCode) => {
      _.each(value, (string) => {
        if(_.includes(this.matches.toString(), string)){
          keyCode = _keyCode;
        }
      });
    });

    let request = this.httpClient.post(
      link,
      {keyCode},
      {headers: { 'Content-Type': 'application/json' }}
    );

    request
      .subscribe(data => {
        console.log(data);
      })
  }
}
