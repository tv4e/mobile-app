import { Component } from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import Moment from 'moment';
import { Help } from '../help/help';
import { Video } from '../video/video';
import { AlertController } from 'ionic-angular';
import _ from 'lodash';
import { Storage } from '@ionic/storage';
import { LocalNotifications } from '@ionic-native/local-notifications';

@Component({
  selector: 'page-home',
  templateUrl: 'recommendations.html'
})

export class Recommendations {
  serial : string;
  videosRaw: Observable<any>;
  videosRaw2: Observable<any>;
  videos = [];
  rootNavCtrl: NavController;

  constructor(private alertCtrl: AlertController,
              public navParams: NavParams,
              public httpClient: HttpClient,
              private storage: Storage,
              private localNotifications: LocalNotifications,
  ) {
    storage.get("serial").then((val) => {
      if(!_.isEmpty(val)){
        this.serial = val;
        this.initializeItems();
      } else{
        this.rootNavCtrl.push(Help);
      }
    });

    //get a reference to the NavController of super-tabs
    this.rootNavCtrl = navParams.get('rootNavCtrl');
  }

  initializeItems(callback = null) {
    this.videos = [];
    this.videosRaw = this.httpClient.get(`http://api_mysql.tv4e.pt/api/mobile/recommendations/${this.serial}`);

    this.videosRaw
      .subscribe(videosRaw => {

        Object.keys(videosRaw).forEach(key => {
          let minute = (Math.floor(videosRaw[key].duration / 60)).toString();
          let seconds = (videosRaw[key].duration - Math.floor(videosRaw[key].duration / 60) * 60).toString();
          let asgieName = this._getAsgie(videosRaw[key].asgie_id);
          minute = minute.length == 1 ? `0${minute}` : minute;
          seconds = seconds.length == 1 ? `0${seconds}` : seconds;
          videosRaw[key].duration = `${minute}:${seconds}`;
          videosRaw[key].date = this._getSentDate(videosRaw[key].date);
          videosRaw[key].asgieName = asgieName;

          this.videos.push(videosRaw[key]);
        });

        console.log(this.videos);
        localStorage.setItem("videosOld", JSON.stringify(this.videos));

        if(callback){
          callback();
        }
      })

    // Check for new videos every minute
    let refresh = setInterval(() => {
      this._checkVideos();
    }, 60000);
  }

  _checkVideos(){
    // Make new request
    let videosNew = [];
    this.videosRaw2 = this.httpClient.get('http://api_mysql.tv4e.pt/api/mobile/recommendations/'+this.serial);

    this.videosRaw2
      .subscribe(videosRaw2 => {
        Object.keys(videosRaw2).forEach(key => {
          let minute = (Math.floor(videosRaw2[key].duration / 60)).toString();
          let seconds = (videosRaw2[key].duration - Math.floor(videosRaw2[key].duration / 60) * 60).toString();
          minute = minute.length == 1 ? `0${minute}` : minute;
          seconds = seconds.length == 1 ? `0${seconds}` : seconds;
          videosRaw2[key].duration = `${minute}:${seconds}`;
          videosRaw2[key].date = this._getSentDate(videosRaw2[key].date);

          videosNew.push(videosRaw2[key]);
        });
      })

    // Retrieve old list from local storage
    let oldV = JSON.parse(localStorage.getItem("videosOld"));

    // Compare old list with new list using videoID and retrive first result
    let diff = _.differenceBy(videosNew, oldV, 'informative_video_id')[0];
    // console.log(diff);

    // If there is a new video fire local notification and save new list
    if (!_.isEmpty(diff)){
      this.localNotifications.schedule({
        title: 'Tem novos vídeos disponíveis',
        text: diff['title'],
      });

      this.videos = videosNew;
      localStorage.setItem("videosOld", JSON.stringify(videosNew));
    }
  }

  _getSentDate(sent_at) {
    // let hours = Moment.duration(Moment(new Date()) - Moment(sent_at)).asSeconds();
    let hours = Moment(new Date()).diff(Moment(sent_at), 'seconds');
    let hoursNow = Moment.duration(Moment(new Date()).format("HH:mm:ss")).asSeconds();
    let sentDate = '';

    if (hours > hoursNow) {
      // 172800 is 48 hours in seconds
      if (hours + hoursNow >= 172800) {
        let days = Math.round(Moment.duration(hours, 'seconds').asDays());
        sentDate = `Há ${days} dias`;
      } else {
        sentDate = "Ontem";
      }
    } else if (hours < hoursNow) {
      sentDate = "Hoje";
    } else if (hours == hoursNow) {
      sentDate = "Hoje";
    }

    return sentDate;
  }

  getItems(ev: any) {
    this.initializeItems(()=>{
      let val = ev.target.value;

      // if the value is an empty string don't filter the items
      if (val && val.trim() != '') {
        this.videos = this.videos.filter((video) => {
          return (video.title.toLowerCase().indexOf(val.toLowerCase()) > -1);
        })
        console.log(this.videos);
      }
    });
  }

  presentConfirm(url, id) {

    console.log(id);
    console.log(url);

    let alert = this.alertCtrl.create({
      title: 'Como ver o vídeo?',
      cssClass: 'bigButton',
      buttons: [
        {
          text: 'Ver no telemóvel',
          handler: () => {
            this.rootNavCtrl.push(Video, {url: url, serial:this.serial, id:id});
          }
        },
        {
          text: 'Ver na TV',
          handler: () => {
            let link = 'http://api_mysql.tv4e.pt/api/mobile/pushvideotv/'+ this.serial +'?informative_video_id='+id;

            let request = this.httpClient.get(link);
            request
              .subscribe(data => {
                console.log(data);
              })
          }
        },
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    alert.present();
  }

  ionViewWillEnter(){
    this._checkVideos();
  }
  _getAsgie(asgieID) {

    let asgie = "";

    switch(asgieID) {
      case "1":
        asgie = "Saúde"
        break;
      case "2":
        asgie = "Sociais"
        break;
      case "3":
        asgie = "Finanças"
        break;
      case "4":
        asgie = "Cultura"
        break;
      case "5":
        asgie = "Segurança"
        break;
      case "6":
        asgie = "Autárquicos"
        break;
      case "7":
        asgie = "Transportes"
        break;
      default:
        asgie = ""
    }

    return asgie;
  }

}
