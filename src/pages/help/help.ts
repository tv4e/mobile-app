import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner';
import { Storage } from '@ionic/storage';
import _ from 'lodash';

@Component({
  selector: 'page-home',
  templateUrl: 'help.html'
})
export class Help {
  scanData : string;
  options : BarcodeScannerOptions;
  login : boolean;

  slides = [
    {
      title: "Bem vindo ao +TV4E mobile!",
      description: "A aplicação <b>+TV4E</b> na palma da sua mão! " +
      "Agora pode aceder aos seus vídeos informativos a partir do seu telemóvel.",
      image: "assets/imgs/ica-slidebox-img-1.png",
    },
    {
      title: "Como navegar na aplicação?",
      description: "Navegar no <b>+TV4E mobile</b> é simples, pode alternar entre " +
      "a sua biblioteca e os vídeos recomendados  " +
      "<b>arrastando o dedo horizontalmente.</b>",
      image: "assets/imgs/ica-slidebox-img-2.png",
    },
    {
      title: "Como ver vídeos informativos?",
      description: "A aplicação <b>+TV4E mobile</b> permite-lhe visualizar vídeos " +
      "informativos no seu telemóvel" +
      " ou na sua televisão.",
      image: "assets/imgs/ica-slidebox-img-5.png",
    }
  ];

  // Tutorial
  slides2 = [
    {
      title: "Como ver vídeos?",
      description: "Para <b>ver um vídeo</b> clique no vídeo que deseja ver " +
      "e escolha se quer ver no <b>telemóvel</b> ou no <b>televisor.</b>",
      image: "assets/imgs/tutorial-1.png",
    },
    {
      title: "Como sair de um vídeo?",
      description: "Para <b>sair do vídeo</b> prima o botão <b>Sair</b> no canto superior direito do ecrã.",
      image: "assets/imgs/tutorial-2.png",
    },
    {
      title: "Como navegar na aplicação?",
      description: "Para <b>navegar</b> clique nos botões no topo da aplicação" +
      " ou <b>deslize</b> o seu dedo horizontalmente no ecrã.",
      image: "assets/imgs/tutorial-3.png",
    },
    {
      title: "Como pesquisar um vídeo?",
      description: "Para pesquisar vídeos use o <b>campo de pesquisa</b> e escreva o título do vídeo. " +
      "<b>Não é necessário escrever o título completo.</b>",
      image: "assets/imgs/tutorial-4.png",
    }
  ];

  constructor(public navCtrl:NavController, private barcodeScanner: BarcodeScanner, private storage: Storage) {
    // Check login
    storage.get("serial").then((val) => {
      if(!_.isEmpty(val)){
        this.login = true;
      } else{
        this.login = false
      }
    });
  }

  public openMain(){
    this.navCtrl.push(TabsPage);
  }

  scan(){
    this.options = {
      prompt : "Aponte a camâra para o código QR"
    }
    this.barcodeScanner.scan(this.options).then((barcodeData) => {
      this.scanData = barcodeData.text;
      this.storage.set("serial", this.scanData);
      // this.storage.set("serial", '9710464146166460');

      this.navCtrl.setRoot(TabsPage);
      this.navCtrl.popToRoot();
    }, (err) => {
      console.log("Error occured : " + err);
    });
  }
}
