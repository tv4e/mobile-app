import { Component } from '@angular/core';
import { NavController, NavParams} from 'ionic-angular';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import Clappr from 'clappr';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpClient } from '@angular/common/http';

@Component({
  templateUrl: 'video.html'
})
export class Video {
  url = "";
  serial = "";
  id = "";
  seen = 0;

  constructor(
              public navParams: NavParams,
              private screenOrientation: ScreenOrientation,
              private statusBar: StatusBar,
              public httpClient: HttpClient,
              private navCtrl:NavController
  ) {
    this.url = navParams.get('url');
    this.serial = navParams.get('serial');
    this.id = navParams.get('id');
  }

  ionViewDidLoad() {
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE);
    this.statusBar.hide();

    var player = new Clappr.Player({
      source: this.url,
      parentId: "#clapprPlayer",
      autoPlay:true,
      chromeless: true,
      height:"100vh", width:"100%",
      events:{
        onTimeUpdate: (time) => this.seen=time,
        onEnded: ()=> {

          let link = 'http://api_mysql.tv4e.pt/api/mobile/seen/'+ this.serial +'?informative_video_id='+this.id+'&seen=100';

          let request = this.httpClient.get(link);
          request
            .subscribe(data => {
              console.log(data);
            })

          let myNode = document.getElementById("clapprPlayer");
          while (myNode.firstChild) {
            myNode.removeChild(myNode.firstChild);
          }

          this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
          this.statusBar.show();
          this.navCtrl.pop();
        }
      }
    });
  }

  ionViewDidLeave(){
    console.log('exit vid')

    let link = 'http://api_mysql.tv4e.pt/api/mobile/seen/'+ this.serial +'?informative_video_id='+this.id+'&seen='+this.seen;

    let request = this.httpClient.get(link);
    request
      .subscribe(data => {
        console.log(data);
      })

    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    this.statusBar.show();
  }

  closeVideo() {
    console.log('fixe');
    this.navCtrl.pop();
  }
}
