import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';
import _ from 'lodash';
import { TabsPage } from '../pages/tabs/tabs';
import { Help } from '../pages/help/help';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  // rootPage:any = TabsPage;
  rootPage:any;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private storage: Storage) {
    platform.ready().then(() => {

      statusBar.backgroundColorByHexString('#000000');
      splashScreen.hide();

      // OneSignal Code start:
      // Enable to debug issues:
      // window["plugins"].OneSignal.setLogLevel({logLevel: 4, visualLevel: 4});

      // var notificationOpenedCallback = function(jsonData) {
      //   console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
      // };
      //
      // window["plugins"].OneSignal
      //   .startInit("8151b9b2-4e3d-44ae-8c36-49c8e8e4822d", "791264652015")
      //   .handleNotificationOpened(notificationOpenedCallback)
      //   .endInit();
    });

    // storage.set("serial", "4abb55a5dfbe7634");

    storage.get("serial").then((val) => {
      if(!_.isEmpty(val)){
        this.rootPage = TabsPage;
      } else{
        this.rootPage = Help;
      }
    });
  }
}
