import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { StreamingMedia } from '@ionic-native/streaming-media';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { SuperTabsModule } from 'ionic2-super-tabs';
import { SpeechRecognition } from '@ionic-native/speech-recognition';

import { AboutPage } from '../pages/about/about';
import { HomePage } from '../pages/home/home';
import { Recommendations } from '../pages/recommendations/recommendations';
import { Help } from '../pages/help/help';
import { Video } from '../pages/video/video';
import { TabsPage } from '../pages/tabs/tabs';
import { IonicStorageModule } from '@ionic/storage';
import { LocalNotifications } from '@ionic-native/local-notifications';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ScreenOrientation } from '@ionic-native/screen-orientation';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    HomePage,
    Help,
    Video,
    Recommendations,
    TabsPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    SuperTabsModule,
    SuperTabsModule.forRoot(),
    IonicStorageModule.forRoot(),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    HomePage,
    Help,
    Video,
    Recommendations,
    TabsPage
  ],
  providers: [
    StatusBar,
    LocalNotifications,
    ScreenOrientation,
    SplashScreen,
    BarcodeScanner,
    StreamingMedia,
    SpeechRecognition,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
